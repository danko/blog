class CommentsController < ApplicationController
	
	#Аутентификация на удаление
	http_basic_authenticate_with name: "d", password: "s", only: :destroy
	
	#Создать
	def create
		@article = Article.find(params[:article_id])
		@comment = @article.comments.create(comment_params)
		redirect_to article_path(@article)
	end
	
	#Удалить
	def destroy
		@article = Article.find(params[:article_id])
		@comment = @article.comments.find(params[:id])
		@comment.destroy
		redirect_to article_path(@article)
	end
	
	private
		def comment_params
			params.require(:comment).permit(:commenter, :body)
		end
end
